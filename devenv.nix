# Docs: https://devenv.sh/basics/
{ pkgs, ... }: {
  languages = {
    # Docs: https://devenv.sh/languages/
    nix.enable = true;
    python = {
      enable = true;
      package = pkgs.python3.withPackages (ps: [
        #ps.matplotlib
        ps.tkinter
      ]);
      venv = {
        enable = true;
        requirements = ''
          librosa>=0.9.1
          numpy>=1.20.0
          webrtcvad>=2.0.10
          torch>=1.0.1
          scipy>=1.2.1
          typing
          matplotlib>=3.0.0
          sounddevice
          tqdm
          umap-learn

          # setuptools
          # click>=6.7
          # colorama>=0.3
          # pyyaml>=5,<7
          # toml>=0.9
          # ZODB>=5.4
          # zodbpickle>=2
          # cfgv>=2.0.0
          # identify>=1.0.0
          # nodeenv>=0.11.1
          # virtualenv>=20.10.0
          # librosa>=0.9.1
          # #numpy>=1.20.0
          # #webrtcvad>=2.0.10
          # #torch>=1.0.1
          # #scipy>=1.2.1
          # #typing
          # #resemblyzer
          # git+https://github.com/resemble-ai/Resemblyzer.git
        '';
      };
    };
  };

  packages = with pkgs; [
    # Search for packages: https://search.nixos.org/packages?channel=unstable&query=cowsay
    # (note: this searches on unstable channel, be aware your nixpkgs flake input might be on a release channel)

    gcc
    zlib
    glibc
    stdenv.cc.cc.lib # for python stuff
    python310Packages.pygobject3
    cairo
    #portaudio - breaks python - SIGBUS
  ];

  scripts = {
    # Docs: https://devenv.sh/scripts/
  };

  difftastic.enable = true; # https://devenv.sh/integrations/difftastic/

  pre-commit.hooks = {
    # Docs: https://devenv.sh/pre-commit-hooks/
    # available pre-configured hooks: https://devenv.sh/reference/options/#pre-commithooks
    # adding hooks which are not included: https://github.com/cachix/pre-commit-hooks.nix/issues/31

    nil.enable = true; # nix check
    nixpkgs-fmt.enable = true; # nix formatting
  };
}
